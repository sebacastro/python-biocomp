# Programación en Python para Biología Computacional

## Universidad Nacional de Quilmes - Octubre 2019

## Cronograma de actividades

| día            | hora     | actividad                                       |
|----------------|----------|-------------------------------------------------|
| lunes 7/10     | 9-13 hs  | *Asistencia configuración (opcional)*           |
|                | 13-18 hs | Clase I. Bioinformática                         |
|                |          | Clase II. La Terminal de Unix                   |
|                |          | Clase III. Control de versiones                 |
| martes 8/10    | 9-13 hs  | Clase IV. Python: Variables y control de flujo  |
|                | 13-18 hs | Clase V. Python : Funciones, archivos y errores |
| miércoles 9/10 | 9-13 hs  | *Práctica asistida (opcional)*                  |
|                | 13-18 hs | Clase VI. Python: manejo de datos               |
| jueves 10/10   | 9-13 hs  | *Práctica asistida (opcional)*                  |
|                | 13-18 hs | Clase VII. Python: visualización de datos       |
| viernes 10/10  | 9-13 hs  | *Práctica asistida (opcional)*                  |
|                | 13-18 hs | Clase VIII. Python para Biología                |

